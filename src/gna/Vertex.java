package gna;

import libpract.Position;

public class Vertex {

	private Position position = new Position (0,0);
	
	public Vertex(Position position){
		this.position = position;
	}
	
	public Position getPosition(){
		return position;
	}
	public boolean equals(Vertex vertex){
		return vertex.getPosition().equals(position);
	}
	public String toString(){
		String string = position.toString();
		return string;
		
	}
}

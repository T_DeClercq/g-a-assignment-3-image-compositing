package gna;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import libpract.*;

/**
 * Implement the methods stitch, seam and floodfill.
 * @author Thomas De Clercq
 */
public class Stitcher
{
	long startTime = System.nanoTime();
	/**
	 * Return the sequence of positions on the seam. The first position in the
	 * sequence is (0, 0) and the last is (width - 1, height - 1). Each position
	 * on the seam must be adjacent to its predecessor and successor (if any).
	 * Positions that are diagonally adjacent are considered adjacent.
	 *
	 * Remark: Position (x, y) corresponds to the pixels image1[x][y] and image2[x][y].
	 * 
	 * image1 and image2 are both non-null and have equal dimensions.
	 */
	public List<Position> seam(int[][] image1, int[][] image2) {
		startTime = System.nanoTime();
		int length = 0;
		int width = 0;
		if (image1.length == image2.length){
			length = image1.length;
			if (image1[0].length == image2[0].length){
				width = image1[0].length;
			}
		}
		else throw new RuntimeException("seems like we have a problem");
		
		
		List<Position> vertices = new LinkedList<Position>();
		List<Edge> edges = new LinkedList<Edge>();
		
		for (int x = 0; x < width; x++){
			for (int y = 0; y < length; y++){
				vertices.add(new Position(x,y));
			}
		}
		System.out.println("Creation of empty array: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
		
		startTime = System.nanoTime();
		for (Position vertex:vertices){
			//System.out.println((vertices.indexOf(vertex)+1) + "/" + (vertices.size()));
			//System.out.println("Calculating neighbours for " + vertex.getPosition().getX() + "x" + vertex.getPosition().getY());
			int topLeft;
			int top;
			int topRight;
			int left;
			int right;
			int downLeft;
			int down;
			int downRight;
			List<Position> neighbours = new LinkedList<Position>();
			if ((vertex.getX()-1)>-1 && (vertex.getY()-1)>-1){
			topLeft = (vertex.getX()-1)*width + vertex.getY() -1;
			neighbours.add(vertices.get(topLeft));}
			if ((vertex.getY()-1)>-1){
			top = vertex.getX()*width + vertex.getY() -1;
			neighbours.add(vertices.get(top));}
			if ((vertex.getX()+1)<width && (vertex.getY()-1)>-1){
			topRight = (vertex.getX()+1)*width + vertex.getY() -1;
			neighbours.add(vertices.get(topRight));}
			if ((vertex.getX()-1)>-1){
			left = vertex.getX()*width -1 + vertex.getY();
			neighbours.add(vertices.get(left));}
			if ((vertex.getX()+1)<width){
			right = (vertex.getX()+1)*width + vertex.getY();
			neighbours.add(vertices.get(right));}
			if ((vertex.getX()-1)>-1 && (vertex.getY()+1)<width){
			downLeft = (vertex.getX()-1)*width + vertex.getY() +1;
			neighbours.add(vertices.get(downLeft));}
			if ((vertex.getY()+1)<width){
			down = vertex.getX()*width + vertex.getY() +1;
			neighbours.add(vertices.get(down));}
			if ((vertex.getX()+1)<width && (vertex.getY()+1)<width){
			downRight = (vertex.getX()+1)*width + vertex.getY() +1;
			neighbours.add(vertices.get(downRight));}
			
			
			
			for (Position adjacentVertex:neighbours){
				if (vertex.isAdjacentTo(adjacentVertex)){
					int x = vertex.getX();
					int y = vertex.getY();
					int weight = ImageCompositor.pixelSqDistance(image1[x][y], image2[x][y]);
					Edge edge = new Edge(vertex,adjacentVertex, weight);
					edges.add(edge);
				}
			}
		}
		System.out.println("Adding edges: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
		Graph image = new Graph(vertices, edges);
		startTime = System.nanoTime();
		Dijkstra algorithm = new Dijkstra(image);
		//Position startPosition = new Position(0,0);
		Position startVertex = vertices.get(0);
		System.out.println(">>	Running Dijkstra...");
		algorithm.run(startVertex);
		System.out.println("Dijkstra: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
		//Position finalPosition = new Position(width - 1, length - 1);
		startTime = System.nanoTime();
		Position target = vertices.get(vertices.size()-1);
		System.out.println(">>	Finding Path...");
		LinkedList<Position> resultVertici= algorithm.getPath(target);
		System.out.println("Pathfinding: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
		return resultVertici;
	}
	
	/**
	 * Apply the floodfill algorithm described in the assignment to mask. You can assume
	 * the mask contains a seam from the upper left corner to the bottom right corner.
	 *
	 * The seam is represented using Stitch.SEAM and all other positions contain the default
	 * value Stitch.EMPTY. So your algorithm must replace all Stitch.EMPTY values with either
	 * Stitch.IMAGE1 or Stitch.IMAGE2.
	 *
	 * Positions left to the seam should contain Stitch.IMAGE1, and those right to the seam
	 * should contain Stitch.IMAGE2. Run `ant test` to test if your implementation does this
	 * correctly!
	 */
	public void floodfill(Stitch[][] mask) {
		Stack<Position> positions = new Stack<Position>();
		positions.push(new Position(mask.length-1,0));
	    while (!positions.isEmpty()){
	        Position position = positions.pop();
	        int x = position.getX();
	        int xmineen = position.getX()-1;
	        int xpluseen = position.getX()+1;
	        int y = position.getY();
	        int ymineen = position.getY()-1;
	        int ypluseen = position.getY()+1;
	        if (mask[position.getX()][position.getY()]== Stitch.EMPTY){
	            mask[position.getX()][position.getY()] = Stitch.IMAGE1;
	            if (xmineen > -1){
	            	positions.push(new Position(xmineen, y));
	            }
	            if (xpluseen < mask.length){
	            	positions.push(new Position(xpluseen, y));
	            }
	            if (ymineen > -1){
            		positions.push(new Position(x, ymineen));
            	}
            	if (ypluseen < mask.length){
            		positions.push(new Position(x, ypluseen));
            	}
//		        System.out.println("Flooding img1 in " + position.getX() + "|" + position.getY());
	        }
	    }
	    if (!positions.isEmpty()){throw new RuntimeException ("Should not happen");}
	    positions.push(new Position(0,mask[0].length-1));
	    while (!positions.isEmpty()){
	        Position position = positions.pop();
	        int x = position.getX();
	        int xmineen = position.getX()-1;
	        int xpluseen = position.getX()+1;
	        int y = position.getY();
	        int ymineen = position.getY()-1;
	        int ypluseen = position.getY()+1;
	        if (mask[position.getX()][position.getY()]==Stitch.EMPTY){
	            mask[position.getX()][position.getY()] = Stitch.IMAGE2;
	            if (xmineen > -1){
	            	positions.push(new Position(xmineen, y));
//	            	System.out.println("pushed xmineen,y");
	            }
	            if (xpluseen < mask.length){
	            	positions.push(new Position(xpluseen, y));
//	            	System.out.println("pushed xpluseen,y");
	            }
	            if (ymineen > -1){
            		positions.push(new Position(x, ymineen));
//           		System.out.println("pushed x, ymineen");
            	}
            	if (ypluseen < mask.length){
            		positions.push(new Position(x, ypluseen));
//            		System.out.println("pushed x, ypluseen");
            	}
//		        System.out.println("Flooding img2 in " + position.getX() + "|" + position.getY());
	        }        
	    }
}		
	
	

	/**
	 * Return the mask to stitch two images together. The seam runs from the upper
	 * left to the lower right corner, where in general the rightmost part comes from
	 * the first image (but remember that the seam can be complex, see the spiral example
	 * in the assignment). A pixel in the mask is Stitch.IMAGE1 on the places where
	 * image1 should be used, and Stitch.IMAGE2 where image2 should be used. On the seam
	 * record a value of Stitch.SEAM.
	 * 
	 * ImageCompositor will only call this method (not seam and floodfill) to
	 * stitch two images.
	 * 
	 * image1 and image2 are both non-null and have equal dimensions.
	 */
	public Stitch[][] stitch(int[][] image1, int[][] image2) {
		// use seam and floodfill to implement this method
		int length = 0;
		int width = 0;
		if (image1.length == image2.length){
			length = image1.length;
			if (image1[0].length == image2[0].length){
				width = image1[0].length;
			}
		}
		else throw new RuntimeException("seems like we have a problem");
		System.out.println("Creating mask...");
		Stitch[][] mask = new Stitch[length][width];
		System.out.println("Finding seam...");
		List<Position> seam = seam(image1,image2);
		System.out.println("Seam found!");
		for (int y = 0; y < length; y++){
			for (int x = 0; x < width; x++){
				mask[x][y] = Stitch.EMPTY;
			}
		}
		System.out.println("Filled an empty image.");
		for (Position position : seam){
			mask[position.getX()][position.getY()] = Stitch.SEAM;
		}
		System.out.println("Imported stitch.");
		System.out.println("Filling up the image...");
		floodfill(mask);		
		System.out.println("Done!");
		return mask;
	}
}



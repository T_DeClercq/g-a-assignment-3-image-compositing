package gna;

import libpract.Position;

public class Edge {

	  private final Position source;
	  private final Position destination;
	  private final int weight; 
	  
	  public Edge(Position source, Position destination, int weight) {
	    this.source = source;
	    this.destination = destination;
	    this.weight = weight;
	  }
	  
	  public Position getDestination() {
	    return destination;
	  }

	  public Position getSource() {
	    return source;
	  }
	  public int getWeight() {
	    return weight;
	  }
	  public String toString(){
		  String string = this.getSource() + " --" + this.getWeight() + "--> " + this.getDestination();
		  return string;
	  }
}

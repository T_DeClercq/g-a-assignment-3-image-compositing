package gna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import libpract.Position;

public class Dijkstra {
  private final List<Position> nodes;
  private final List<Edge> edges;
  private Set<Position> settledNodes;
  private Set<Position> unSettledNodes;
  private Map<Position, Position> predecessors;
  private Map<Position, Integer> distance; 
  private int width;
  private long startTime;

  public Dijkstra(Graph graph) {
    this.nodes = new LinkedList<Position>(graph.getPositions());
    this.edges = new ArrayList<Edge>(graph.getEdges());
    this.width = (int) Math.round(Math.pow(graph.getPositions().size(), 0.5));
  }
  public void run(Position source) {
	startTime = System.nanoTime();
    settledNodes = new HashSet<Position>();
    unSettledNodes = new HashSet<Position>();
    distance = new HashMap<Position, Integer>();    
    predecessors = new HashMap<Position, Position>(); 
    distance.put(source, 0);
    unSettledNodes.add(source);
    while (unSettledNodes.size() > 0) {
      Position node = getMinimum(unSettledNodes);   
      settledNodes.add(node);                     
      unSettledNodes.remove(node);                // remove from unsettled set
      findMinimalDistances(node);
    }
	System.out.println(">>> Dijkstra.run: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
  }

  // Given a node, go through its adjacent nodes. Recalculate the shortest
  // distance held in 'distance' Map where temporary distance from source is
  // held. Then add all adjacent node into unsettled set. 
  private void findMinimalDistances(Position node) {
	startTime = System.nanoTime();
    List<Position> adjacentNodes = getNeighbors(node);
    for (Position target : adjacentNodes) {
      if (getShortestDistance(target) > getShortestDistance(node)
          + getDistance(node, target)) {
        distance.put(target, getShortestDistance(node)
            + getDistance(node, target));
        predecessors.put(target, node); 
        unSettledNodes.add(target);   
      }
    }
	//System.out.println(">>> Dijkstra.findMinimalDistances: " + (System.nanoTime() - startTime) / Math.pow(10, 9));

  }

  private int getDistance(Position node, Position target) {
    for (Edge edge : edges) {
      if (edge.getSource().equals(node)
          && edge.getDestination().equals(target)) {
    	  
        return edge.getWeight();
      }
    }
    throw new RuntimeException("Should not happen");
    
  }

  private List<Position> getNeighbors(Position vertex) {
	  startTime = System.nanoTime();
    List<Position> neighbors = new ArrayList<Position>();
    for (Edge edge : edges) {
      if (edge.getSource().equals(vertex)
          && !isSettled(edge.getDestination())) {
        neighbors.add(edge.getDestination());
      }
    }
    /*
    List<Position> neighbours = new LinkedList<Position>();
	if ((vertex.getX()-1)>-1 && (vertex.getY()-1)>-1){
	int topLeft = (vertex.getX()-1)*width + vertex.getY() -1;
	neighbours.add(nodes.get(topLeft));}
	if ((vertex.getY()-1)>-1){
	int top = vertex.getX()*width + vertex.getY() -1;
	neighbours.add(nodes.get(top));}
	if ((vertex.getX()+1)<width && (vertex.getY()-1)>-1){
	int topRight = (vertex.getX()+1)*width + vertex.getY() -1;
	neighbours.add(nodes.get(topRight));}
	if ((vertex.getX()-1)>-1){
	int left = vertex.getX()*width -1 + vertex.getY();
	neighbours.add(nodes.get(left));}
	if ((vertex.getX()+1)<width){
	int right = (vertex.getX()+1)*width + vertex.getY();
	neighbours.add(nodes.get(right));}
	if ((vertex.getX()-1)>-1 && (vertex.getY()+1)<width){
	int downLeft = (vertex.getX()-1)*width + vertex.getY() +1;
	neighbours.add(nodes.get(downLeft));}
	if ((vertex.getY()+1)<width){
	int down = vertex.getX()*width + vertex.getY() +1;
	neighbours.add(nodes.get(down));}
	if ((vertex.getX()+1)<width && (vertex.getY()+1)<width){
	int downRight = (vertex.getX()+1)*width + vertex.getY() +1;
	neighbours.add(nodes.get(downRight));}
    
    */
	//System.out.println(">>> Dijkstra.getNeighbours: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
    return neighbors;
  }

  private Position getMinimum(Set<Position> vertexes) {
	  startTime = System.nanoTime();
    Position minimum = null;
    for (Position vertex : vertexes) {
      if (minimum == null) {
        minimum = vertex;
      } else {
        if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
          minimum = vertex;
        }
      }
    }
	//System.out.println(">>> Dijkstra.getMinimum: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
    return minimum;
  }

  private boolean isSettled(Position vertex) {
    return settledNodes.contains(vertex);
  }

  private int getShortestDistance(Position destination) {
    Integer d = distance.get(destination);
    if (d == null) {
      return Integer.MAX_VALUE;
    } else {
      return d;
    }
  }

  public LinkedList<Position> getPath(Position target) {
	  startTime = System.nanoTime();
    LinkedList<Position> path = new LinkedList<Position>();
    Position step = target;
    if (predecessors.get(step) == null) {
    	System.out.println("help");
      return null;
    }
    path.add(step);
    while (predecessors.get(step) != null) {
      step = predecessors.get(step);
      path.add(step);
    }
    Collections.reverse(path);
	System.out.println(">>> Dijkstra.getPath: " + (System.nanoTime() - startTime) / Math.pow(10, 9));
    return path;
  }

}  
package gna;
import java.util.List;

import libpract.Position;

public class Graph {
	
	  private final List<Position> vertexes;
	  private final List<Edge> edges;

	  public Graph(List<Position> vertexes, List<Edge> edges) {
	    this.vertexes = vertexes;
	    this.edges = edges;
	  }

	  public List<Position> getPositions() {
	    return vertexes;
	  }

	  public List<Edge> getEdges() {
	    return edges;
	  }
	  
	  
}
